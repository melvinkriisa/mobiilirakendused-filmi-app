

if (typeof (Storage) !== "undefined") {
    if (localStorage.getItem("movies") === null) {
        var myMov = [];
        localStorage.setItem("movies", JSON.stringify(myMov));
    } else {
        myMov = JSON.parse(localStorage.getItem("movies"));
    
    }
} else {
    alert("LocalStorage ei toetata! Kasutage uut brauserit!");
}


var globMid = "";
var globPoster = "";
var globRel_date = "";
var globTitle = "";
var globOverview = "";
var globFlag = false;


function openTab(evt, tabID) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabID).style.display = "block";
    
    //API key - 787cc34a3c6aab3714fa5f87d7e2b6e4 
}

function getTopMov() {
    $('#top').empty();
    $('#top').append('<h3>Hetkel populaarsed filmid</h3>');
    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=787cc34a3c6aab3714fa5f87d7e2b6e4",
    "method": "GET",
    "headers": {},
    "data": "{}"
    }
    $.ajax(settings).done(function (response) {
        for (var key in response) {
            if (response.hasOwnProperty(key)) {
                if (key == "results") {
                    var i = 0;
                    for (let item of response[key]) {
                        var rel_date = "";
                        var title = "";
                        var poster = "";
                        var mid = "";
                        
                        if (response[key].indexOf(item) == i) { 
                            $.each(item, function(key, value) {   
                                if (key == "release_date") {
                                    var splitVal = value.split("-", 3);
                                    rel_date += splitVal[0];
                                } else if (key == "poster_path") {
                                    poster += value;
                                    poster = "<img src=https://image.tmdb.org/t/p/w500" + poster + " style=width:170px;height:190px;>";
                                } else if (key == "id") {
                                    mid += value;
                                } else if (key == "title") {
                                    title += value;       
                                    $('#top').append('<a href="javascript:void(0)" onclick="insertInfo(this.innerHTML);" id="' + i +'">' + poster + "<br>" + title + ", " +   rel_date + '<p class="hidemid" id="moid">' + "..." + mid + "..." + '</p></a><br><br>');
                                    
                                    $(".hidemid").hide();
                                    
                                    
                                    i++;
                                }  
                            }); 
                        }
                    }
                }
            }
        }    
    });
}

function watchVideo(id) {
    
    var movKey = "";
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api.themoviedb.org/3/movie/" + id + "/videos?language=en-US&api_key=787cc34a3c6aab3714fa5f87d7e2b6e4",
        "method": "GET",
        "headers": {},
        "data": "{}"
    }

    $.ajax(settings).done(function (response) {
        
        for (var key in response) {
            if (key == "results") {
                var i = 0;
                for (let item of response[key]) {
                    
                    if (response[key].indexOf(item) == i) {
                        $.each(item, function(key, value) {
                            if (key == "key") {
                                movKey += value;
                            }
                            i++;
                            
                        });
                        
                        
                    }
                }
            }
        }
        $("#btn").hide();
        console.log(movKey);
        $('#movInfoP').replaceWith('<iframe id="movInfoP" width="450" height="345" src=' + "https://www.youtube.com/embed/" + movKey + '></iframe>');
    });
}


function searchMov() {
    
    $('#movInfo').empty();
    $('#movInfo').append("<h3>Otsitavad filmid</h3>");
    var formVal = document.getElementById("searchMovIn").value;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api.themoviedb.org/3/search/movie?include_adult=false&page=1&query=" + formVal + "&language=en-US&api_key=787cc34a3c6aab3714fa5f87d7e2b6e4",
        "method": "GET",
        "headers": {},
        "data": "{}"
    }
    
    $.ajax(settings).done(function (response) {
       
        for (var key in response) {
            if (response.hasOwnProperty(key)) {
                if (key == "results") {
                    var i = 0;
                    for (let item of response[key]) {
                        var rel_date = "";
                        var title = "";
                        var poster = "";
                        var mid = "";
                        if (response[key].indexOf(item) == i) {
                            $.each(item, function(key, value) {
                                if (key == "release_date") {
                                    if (value == null || value == "") {
                                        rel_date += "(Aastaarv puudub!)";
                                    } else {
                                        var splitVal = value.split("-", 3);
                                        rel_date += splitVal[0];
                                    }
                                } else if (key == "poster_path") {
                                    if (value == null) {
                                        poster += "(Pilt puudub!)";
                                    } else {
                                        poster += value;
                                        poster = "<img src=https://image.tmdb.org/t/p/w500" + poster + " style=width:170px;height:190px;>";
                                    }
                                } else if (key == "id") {
                                    mid += value;
                                } else if (key == "title") {
                                    title += value;
                                    $('#movInfo').append('<a href="javascript:void(0)" onclick="insertInfo(this.innerHTML);" id="mov ' + i +'">' + poster + "<br>" + title + ", " +   rel_date + '<p class="hidemid" id="moid">' + "..." + mid + "..." + '</p></a><br><br>');
                                    $(".hidemid").hide();
                                    i++;
                                    
                                }
                            });  
                        }
                    }
                }
            }
        }
    });

}

var movCont;
function insertInfo(selectedLink) {
    
    var flag;
    
    var mid;
    
    movCont = selectedLink;
    mid = movCont.split("...");
    mid = mid[1];
    
    for (let item of myMov) {        
        $.each(item, function(key, value) {
            if (key == "id") {
                if (value == mid) {
                    console.log(mid);
                    $("#btn").html("Eemalda listist!");
                    flag = true;
                    globFlag = true;
                    return false;
                } else if (value != mid) {
                    $("#btn").html("Lisa listi!");
                    globFlag = false;
                }
            }           
        });
        
        if (flag == true) {
            break;
        }         
    }
    
    
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api.themoviedb.org/3/movie/"+ mid + "?api_key=787cc34a3c6aab3714fa5f87d7e2b6e4",
        "method": "GET",
        "headers": {},
        "data": "{}"
    }
    
        $.ajax(settings).done(function (response) {
            var rel_date = "";
            var title = "";
            var poster = "";
            var mid = "";
            var overview = "";
            globMid = "";
            globPoster = "";
            globTitle = "";
            globRel_date = "";
            globOverview = "";
            $.each(response, function(key, value) {
                
                    if (key == "id") {
                        mid += value;
                    } else if (key == "overview") {
                        overview += value;
                    } else if (key == "poster_path") {
                        if (value == null) {
                            poster += "(Pilt puudub!)";
                        } else {
                            poster += value; 
                            poster = "<img src=https://image.tmdb.org/t/p/w500" + poster + " style=width:270px;height:310px;>";
                        }
                    } else if (key == "release_date") {
                        if (value == null || value == "") {
                            rel_date += "(Aastaarv puudub!)";
                        } else {
                            var splitVal = value.split("-", 3);
                                rel_date += splitVal[0];
                        }
                    }  else if (key == "title") {
                        title += value;
                        $('#movInfoP').replaceWith('<p id="movInfoP">' + poster + "<br>" + title + ", " +   rel_date + "<br><br>" + overview + '<a href="javascript:void(0)" onclick="watchVideo(' + mid + ');"><br> Vaata trailerit!</a></p>');
                        $("#btn").show();
                        globMid += mid;
                        globPoster += poster;
                        globTitle += title;
                        globRel_date += rel_date;
                        globOverview += overview;
                            
                    }
                });
            
    });
    $(window).scrollTop(0);
    
}
$("#btn").hide();

function addToList() {
    var obj = new Object();
    
    obj["id"] = globMid;
    obj["poster"] = globPoster;
    obj["title"] = globTitle;
    obj["rel_date"] = globRel_date;
    obj["overview"] = globOverview;
    
    console.log(globFlag);
    if (globFlag == false ) {
        myMov.push(obj);
        localStorage.setItem("movies", JSON.stringify(myMov));
        $("#btn").html("Eemalda listist!");
        globFlag = true;
        myMov = JSON.parse(localStorage.getItem("movies")); 
    } else if (globFlag == true) {
        for (let item of myMov) {
            $.each(myMov, function(i){
                if(myMov[i].id === globMid) {
                    myMov.splice(i,1);
                    globFlag = false;
                    return false;
                    
                }
            });
        }
        localStorage.setItem("movies", JSON.stringify(myMov));
        myMov = JSON.parse(localStorage.getItem("movies"));
        $("#btn").html("Lisa listi!");
        
    }
    
}

function deleteFromList(selectedLink) {
    var movTitle = selectedLink;
    movTitle =  movTitle.split("<br>");
    movTitle = movTitle[1];
    movTitle = movTitle.split(", ");
    movTitle = movTitle[0];
    
        for (let item of myMov) {
            $.each(myMov, function(i){
                if(myMov[i].title == movTitle) {
                    myMov.splice(i,1);
                    globFlag = false;
                    return false;
                    
                }
            });
        
        localStorage.setItem("movies", JSON.stringify(myMov));
        myMov = JSON.parse(localStorage.getItem("movies"));
        
    }
}

function showFavList() {
    $('#movInfoFav').empty();
    console.log(myMov);
                
    for (let item of myMov) {
        var rel_date = "";
        var title = "";
        var poster = "";
        var mid = "";
        var overview = "";
        $.each(item, function(key, value) {
            console.log(key);
                    
            if (key == "id") {
                mid += value;
            } else if (key == "poster") {
                poster += value;
            } else if (key == "title") {
                title += value;
            } else if (key == "rel_date") {
                rel_date += value;
            } else if (key == "overview") {
                overview += value;
                $('#movInfoFav').append('<p id="movInfoPfav">' + poster + "<br>" + title + ", " +  rel_date + "<br><br>" + overview + '</p><button id="btn" type="button" onclick="deleteFromList(this.previousSibling.innerHTML);">Eemalda listist!</button><br>');
            }
                        
        });
                
    }
            
}




    



